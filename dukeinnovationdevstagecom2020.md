# Dukeinnovation.dev-stage.com/ Assessment

__<http://dukeinnovation.dev-stage.com/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).



Fantastic work! Thank you remediating so many issues from the previous assessment. Only two remaining issues.



## Links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://web.dev/link-name/).

Links need to tell someone their purpose and where it would take them. "Read more" is not descriptive. Screen readers will just hear will just say "Link Read more". 

__Visual location example:__

http://dukeinnovation.dev-stage.com home example:

![read more problem](assets/read-more.png)


http://dukeinnovation.dev-stage.com/education/ subpage example:

![read more problem](assets/read-more2.png)

There are more examples, but those should suffice to show what to be mindful of.


#### Suggested solution:

Make link text more specific like:

"Read about our Featured Program" 

or add hidden screen reader text by programmatically prepending the title.

```html
<a href="foo">Read more<span class="sr-only">  about our Featured Program"</span></a> 
```

or any other way that makes the link discernible. 



<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,0,DIV,2,FOOTER,0,DIV,0,DIV,3,DIV,0,UL,4,LI,0,A`<br>
Selector:<br>
`a[hrefS="twitter\.com"]`
</details>


---

<br>




## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>


## Focus should not cause an action [WCAG 3.2.1](https://www.w3.org/TR/UNDERSTANDING-WCAG20/consistent-behavior-receive-focus.html)

__I need a human!__ Manual Test: Does the webpage do things the user did not ask it to do?

Description:<br>

Functionality must be predictable as visitors navigate their way through a website. Any component that is able to trigger an event when it receives focus must not cause an action.

__Success!__

---

<br>

## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---

<br>

## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

__Success!__



---

<br>



## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Success!__


---

<br>


## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

Great work has already been done to improve the hamberger menu system. Only one issue remains.

__Visual location:__

![need some ARIA](assets/insufficuent-hover-state.png)

__HTML location:__

```html
<a href="#" class="nav-opener" aria-label="main menu toggle">
    <span></span>
    <span class="sr-only">menu close</span>
</a>
```

__Needs work.__

Expand collapse hamburger button does not communicate state of it's target menu to screen reader users.

#### Suggested solution:

Then the button needs to communicate its state using the ARIA `aria-expanded` attribute.

For example:

The result of adding the ARIA attribute should look something like this:

```html
<nav>
    <a href="#" class="nav-opener" aria-label="main menu toggle" aria-expanded="false">
        <span></span>
        <span class="sr-only">menu close</span>
    </a>
    ...
</nav>
```

The `aria-expanded` attribute needs toggled to describe the state of the target menu.  Meaning, make it  `true` when the menu is activated and visible. False when its hidden.



---

<br>




## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---

<br>



## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Success!__

---


<br>


## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success after a previous issue is resolved.__

---

<br>



## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).

__Success!__

---


<br>






## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Great success!__

---

<br>



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success!__


<br>

## Skip to link / bypass block missing. 

Adding ways to bypass repetitive content allows keyboard users navigate the page more efficiently. [Learn more](https://dequeuniversity.com/rules/axe/2.2/bypass).

__Success!__


---

<br>



## HTML Error

HTML validation errors are fixed.

__Success!__

---

<br>

## Note: Number of mega menu links.

The length and number of a menu items is not an accessibility concern for sighted users. 

For a screen reader users, the audio of reading over the 41 links in the main menu would take over one minute (1:10) with default settings.  The cognitive load for screen reader users to remember 41 links when they are trying to navigate a website is very high.  When there are more than 15-20 menu items special keyboard option are encouraged for the user. This way they can to choose which main menu sub items they want to hear more about.

For example, when a user hits tab it goes across the top menu items.  If the user gets `:focus` to "Social Innovation" they would hit the down arrow and explore that section's sub menu items.

How long is "too long" is a little subjective. If it takes more than a minute to listen to the menu every time the pages loads, its safe to say it is getting too unruly to navigate _for screen reader users_. 

We have also had a few official complaints about long menus that do not provide a method to allow the user to navigate into specific sections.

But adding this type of functionality is rather difficult to add and maintain. If it gets much longer we won't be able to defend it if a complaint is made.

That said, it would be nice to keep it the menu as-is for simplicity sake.  It's about one minute now. So, please be mindful of the number of menu items so we don't need to add all that keyboard complexity. 

__Satisfactory as-is.__


---

<br>

<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.