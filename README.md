Accessibility assessment of https://entrepreneurship.duke.edu/


Assesses the level of the websites accessibility at the time the report was created.
Provides suggestions and guidance on how to remediate those issues.


For more info contact jhc36@duke.edu.