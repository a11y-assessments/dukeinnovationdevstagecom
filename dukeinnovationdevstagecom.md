# Dukeinnovation.dev-stage.com/ Assessment

__<http://dukeinnovation.dev-stage.com/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).







## Links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://web.dev/link-name/).



__Visual location:__

![ not descriptive](assets/logo.png)

__HTML location:__

```html
<a href="http://dukeinnovation.dev-stage.com" class="logo">
  <img width="291" height="48" src="http://dukeinnovation.dev-stage.com/wp-content/uploads/2019/12/logo.png" class="attachment-full size-full" alt="">
  </a>
```

__Needs work.__

The website's logo is missing alt text. Screen reader users will not know how to get to the homepage from other pages.

#### Suggested solution:

Add `alt="Innovation and Entrepreneurship home"`

<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,0,DIV,0,DIV,0,DIV,0,HEADER,0,DIV,0,DIV,0,A`<br>
Selector:<br>
`.justify-content-between > .logo`
</details>

<br>

---

<br>



### Social media icon `<a>` links have no text inside x 5.

__Visual location:__

![ not descriptive](assets/social.png)

__HTML Location__:

```html
<ul class="social-networks">
  <li>
    <a href="mailto:test@test.com" target="_blank">
      <i class="fas fa-envelope" aria-hidden="true"></i>
    </a>
  </li>
  ...
  <li></li>
</ul>
```

Empty links are read as "Link" or not read to a screen reader user at, as a result, they will have no idea what the link does or where it would take them.

#### Suggested solution:

Add invisible screen reader only text to describe its purpose. If the empty link is extraneous consider removing it.

If it is Facebook, add `<span sr-only="facebook">` in the link... and so on .





<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,0,DIV,2,FOOTER,0,DIV,0,DIV,3,DIV,0,UL,4,LI,0,A`<br>
Selector:<br>
`a[hrefS="twitter\.com"]`
</details>

<br>

---

<br>


### FYI: What is 'invisible screen reader only' text?

Screen reader only text is text that is read out-loud to a screen reader user, but not displayed on the screen. Most websites already have a class for this called `.element-invisible` or `.sr-only`. If this website does not, add this class to a stylesheet:

```css
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}
```

Example usage:

```html
<a class="read-more" href="fancy-cats.html">
    Read more
+    <span class="sr-only"> about fancy cats</span>
  </i>
</a>
```

---


<br>




## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>


## Focus should not cause an action [WCAG 3.2.1](https://www.w3.org/TR/UNDERSTANDING-WCAG20/consistent-behavior-receive-focus.html)

__I need a human!__ Manual Test: Does the webpage do things the user did not ask it to do?

Description:<br>

Functionality must be predictable as visitors navigate their way through a website. Any component that is able to trigger an event when it receives focus must not cause an action.

__Needs work.__

Please watch video for explanation:

<video width="640" controls>
  <source src="assets/focus-cause-action.mp4" type="video/mp4">
</video>

#### Suggested solution:

The menu should only expand when the user tells it to. Expectation is that the hamburger menu has focus. And then the user hits the enter or space key.

Modify the event listener. 

---

<br>

## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Needs work.__


Please watch video for explanation:

<video width="640" controls>
  <source src="assets/hidden-menu-reading.mp4" type="video/mp4">
</video>

#### Suggested solution:

When the menu is hidden use `display:none` to ensure keyboard focus is not lost inside the hidden menu.

---

<br>

## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

__Visual location:__

![no focus state on slide buttons](assets/no-focus-state-on-interactive-slideshow.png)

__HTML location:__

```html
<div class="acc-item..." ... >
  ...
</div>
<div class="acc-item red-item" style="display: block; position: absolute; z-index: 3; width: 1315.2px; right: 136px;">
  <a class="acc-opener s2" href="#">
    <span class="text">FEATURED PROGRAM</span>
  </a>
  ...
</div>
<div class="acc-item..." ... >
  ...
</div>
<div class="acc-item..." ... >
  ...
</div>
```

__Needs work.__

"The Bullpen", "Features program", "Features story", "Featured Event" slide buttons are not keyboard focusable.

The buttons will also need to be operable for keyboard-only users.

#### Suggested solution:

Apply the mouse click behavior to the enter key when it has `:focus`.

or try the following workaround:


```js
$('.selector-of-this-type-of-faux-link').keyup(function (e) {
    if (e.keyCode === 13 || e.keyCode === 32) { // key press for WCAG
        $(this).click();
    }
});
```

To test if it is working, tab to the element and hit the enter key to see if it acts exactly like a mouse click (activating the correct slide).

---

<br>



## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Needs work.__

Insufficient hover state. User cannot tell if the button has `:focus` and mouse users may have difficulty knowing if it is being hovered over.

__Visual location:__

![Insufficient hover state example](assets/insufficuent-hover-state.png)

__HTML location:__

```html
<a href="#" class="nav-opener"><span></span></a>
```

#### Suggested solution:

Option A: Increase contrast of hover state. WCAG rule for hover states is 3:1.  

Option B: Add an outline or focus ring.


---

<br>




## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

__Visual location:__

![Insufficient hover state example](assets/insufficuent-hover-state.png)

__HTML location:__

```html
<a href="#" class="nav-opener">
  <span></span>
</a>
```

__Needs work.__

Expand collapse hamburger button does not communicate state to screen reader users.

#### Suggested solution:

The button needs text in it to tell screen reader users what the button does. Add screen reader only text.

Then the button needs to communicate its state using the `aria-expanded` attribute.

For example:

The result of these fixes should look something like this:

```html
<a href="#" class="nav-opener" aria-expanded="false">
  <span sr-only="main menu"></span>
</a>
```

And toggle the `aria-expanded` attribute to `true` when the menu is activated and visible.

Additionally, and somewhat related, there is no "Navigation" Landmark on the page. The easiest way to deal with this would be to wrap the `nav-opener` with a `<nav aria-label="main menu">`

So the end result of both tasks would be:

```html
<nav aria-label="main menu toggle">
  <a href="#" class="nav-opener" aria-expanded="false">
    <span sr-only="main menu toggle"></span>
  </a>
</nav>
```

_Note: Wrapping it here will not surround the actual contents of the menu, however if someone is navigating through the page via Landmarks, the main navigation mega menu would be hidden anyway unless this button activates it, so there would be little advantage to surrounding something that is fully hidden. Currently the hamburger and the main menu are not nested inside the same parent `div`, which would make any other attempt to wrangle them together a rather invasive task for no significant gain in functionality. So, just do the easy thing and wrap the toggle._


---

<br>




## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---

<br>



## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Success after previous issues are resolved.__

---


<br>


## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success after previous issues are resolved.__

---

<br>



## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).

__Success!__

---


<br>






## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Needs work.__

Headings do not skip levels.  Which is good.  But standard practice is one `<h1>` per page. 

https://www.w3.org/WAI/tutorials/page-structure/headings/

https://www.w3.org/TR/2014/REC-html5-20141028/sections.html#headings-and-sections

There are exceptions to this guideline if the headings are nested in specific types of HTML5 element or `role`s, etc. Its easier just to change them than to meet those overly complicated rules.

#### Suggested solution:

Keep the page's title as an `<h1>` and bump the rest down one level respectively.

Visually it does not matter what they look like. They can stay exactly as they appear.

---

<br>



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success pending previously mentioned hamburger issue resolution.__


---

<br>

## Skip to link / bypass block missing. 

Adding ways to bypass repetitive content allows keyboard users navigate the page more efficiently. [Learn more](https://dequeuniversity.com/rules/axe/2.2/bypass).

__Needs work.__

The skip-to bypass block needs to be the first (usually hidden) link on the page. When a user hits the tab key it gives it focus and as a result becomes visible. If the user hits the enter key while it is focused it will jump to the main content area.

![example skip to](assets/skiptobypass.gif)

#### Suggested solution:

Make the skip-to link bypass block jump to the main content area.


Keyboard and screen reader users expect the the skip-to link trigger to be the first focusable element in the `<body>` element.

```css
.element-invisible {
    margin: 0;
    padding: 0;
    width: 1px;
    position: absolute !important;
    clip: rect(1px 1px 1px 1px);
    clip: rect(1px,1px,1px,1px);
    overflow: hidden;
    height: 1px;
}
.element-invisible.element-focusable:active, 
.element-invisible.element-focusable:focus {
    position: static !important;
    clip: auto;
    overflow: visible;
    height: auto;
}
  
```

This will keep the following HTML hidden, and only show it to keyboard only users.  It needs to be the first link on the page.

```html
<body>
+  <div id="skip-link">
+    <a href="#main" class="element-invisible element-focusable">Skip to main content</a>
+  </div>
...
```

`#main` should be an `id` on the page after the main navigation.  If there is no consistent `id` on every page, add one.

__Success criteria__:

You will know it works if:

1. It is invisible when the page loads.

2. When you hit the tab key, it is the first element that receives focus on the page.




---

<br>



## HTML Error

https://validator.w3.org/

Check the `<head>` for invalid HTML.  There is a `<div>` tag in the `<head>`.  HTML5 Block elements are not allowed in the `<head>` of a document.

Fixing that issue, will remove the remaining errors.

The two concerns here are that web browsers and devices can behave unpredictably with invalid code. Assistive technology can also get hung up for the same reason. Plus, it can make the automated checkers and validators report errors that really don't exist.

---

<br>

## Note: Number of mega menu links.

The length and number of a menu items is not an accessibility concern for sighted users. 

For a screen reader users, the audio of reading over the 41 links in the main menu would take over one minute (1:10) with default settings.  The cognitive load for screen reader users to remember 41 links when they are trying to navigate a website is very high.  When there are more than 15-20 menu items special keyboard option are encouraged for the user. This way they can to choose which main menu sub items they want to hear more about.

For example, when a user hits tab it goes across the top menu items.  If the user gets `:focus` to "Social Innovation" they would hit the down arrow and explore that section's sub menu items.

How long is "too long" is a little subjective. If it takes more than a minute to listen to the menu every time the pages loads, its safe to say it is getting too unruly to navigate _for screen reader users_. 

We have also had a few official complaints about long menus that do not provide a method to allow the user to navigate into specific sections.

But adding this type of functionality is rather difficult to add and maintain. If it gets much longer we won't be able to defend it if a complaint is made.

That said, it would be nice to keep it the menu as-is for simplicity sake.  It's about one minute now. So, please be mindful of the number of menu items so we don't need to add all that keyboard complexity. 




---

<br>

<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.